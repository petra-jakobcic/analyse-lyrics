// Get the HTML elements we need.
const form = document.getElementById("form");
const artistInput = document.getElementById("artist-name");
const songInput = document.getElementById("song-title");
const lyricsInput = document.getElementById("lyrics");
const button = document.getElementById("button");
const analysisSection = document.getElementById("section-analysis");

// Write the functions that we need.
/**
 * Creates an object that contains the current
 * values of the input boxes.
 * 
 * @returns {Object} The input values.
 */
function getInputValues() {
    const artist = artistInput.value;
    const song = songInput.value;
    const lyrics = lyricsInput.value;
    
    const inputValues = {
        artist,
        song,
        lyrics
    }

    return inputValues;
}

/**
 * Creates a new analysis.
 * 
 * @param {string} song The song title.
 * @param {string} artist The artist's name.
 * @param {number} numberOfWords The number of words in a song.
 * @param {Array} mostCommonWords The most common word(s) in a song.
 * 
 * @returns {HTMLDivElement} The analysis.
 */
function createAnalysis(song, artist, numberOfWords, mostCommonWords) {
    // Create the elements we need.
    const analysisDiv = document.createElement("div");
    const analysisText1P = document.createElement("p");
    const songP = document.createElement("p");
    const analysisText2P = document.createElement("p");
    const artistP = document.createElement("p");
    const analysisText3P = document.createElement("p");
    const numberOfWordsP = document.createElement("p");
    const analysisText4P = document.createElement("p");
    const commonWordsP = document.createElement("p");
    
    // Set their inner HTML.
    analysisText1P.innerHTML = "the song";
    songP.innerHTML = song;
    analysisText2P.innerHTML = "performed by";
    artistP.innerHTML = artist;
    analysisText3P.innerHTML = "consists of";
    numberOfWordsP.innerHTML = numberOfWords + " word(s)";
    analysisText4P.innerHTML = "the most common word(s) is/are";
    commonWordsP.innerHTML = mostCommonWords;
    
    // Add a class to each new element.
    songP.classList.add("result");
    artistP.classList.add("result");
    numberOfWordsP.classList.add("result");
    commonWordsP.classList.add("result");
    
    // Build the profile.
    analysisDiv.appendChild(analysisText1P);
    analysisDiv.appendChild(songP);
    analysisDiv.appendChild(analysisText2P);
    analysisDiv.appendChild(artistP);
    analysisDiv.appendChild(analysisText3P);
    analysisDiv.appendChild(numberOfWordsP);
    analysisDiv.appendChild(analysisText4P);
    analysisDiv.appendChild(commonWordsP);
    
    return analysisDiv;
}

// When I click on the button,
// the lyrics are analysed and
// a new 'div' gets created
// containing the results of the analysis.
/**
 * Responds to the button click.
 * 
 * @param {Event} carrot The click event.
 * 
 * @returns void
 */
 function formSubmitListener(carrot) {
    carrot.preventDefault(); // prevents the default behaviour of the browser to submit the form

    const values = getInputValues();

    // The 'piping' part:
    // get the info about the song and store it in variables.
    const individualWords = getIndividualWords(lyricsInput.value); // an array
    const numberOfWords = individualWords.length; // a number
    const occurrences = getWordOccurrences(individualWords); // an object with numbers as values
    const highestValue = getTheHighestValue(occurrences); // a number
    const matchingKeys = findKeysByValue(highestValue, occurrences); // an array

    const mostCommonWords = matchingKeys.join(", ");

    // Use the newly created variables as function parametres.
    const analysis = createAnalysis(
        values.song,
        values.artist,
        numberOfWords,
        mostCommonWords,
    );

    analysisSection.appendChild(analysis);

    form.reset();

    button.disabled = true;
}

/**
 * Determines what happens on key press in the lyrics input field.
 *
 * @returns {void}
 */
function handleLyricsInputKeyPress(carrot) {
    if (lyricsInput.value.length > 0) {
        button.disabled = false;
    } else {
        button.disabled = true;
    }
}

// Tie everything together.
form.addEventListener("submit", formSubmitListener);
lyricsInput.addEventListener("keyup", handleLyricsInputKeyPress);
