/**
 * Gets individual words from a song.
 * 
 * @param {string} song The song.
 * 
 * @returns {Array} An array of individual words in lower case.
 */
function getIndividualWords(text) {
    // regex
    const newLines = /\n/g;
    const notLetterOrSpaceOrApostrophe = /[^a-zA-Z\s\']+/g;

    text = text.replace(newLines, ' ');
    text = text.replace(notLetterOrSpaceOrApostrophe, '');

    const individualWords = text.toLowerCase().split(' ');
  
    return individualWords;
}

/**
 * Analyses a list of words and returns an object with the
 * number of occurrences of each word from the list.
 * 
 * @param {Array} listOfWords A list of words.
 * 
 * @returns {Object} An object with word occurrences.
 */
function getWordOccurrences(listOfWords) {
    let wordOccurrences = listOfWords.reduce((tally, currentWord) => {
        tally[currentWord.toLowerCase()] = (tally[currentWord.toLowerCase()] || 0) + 1;

        return tally;
    }, {});

    return wordOccurrences;
}

/**
 * Finds all the object keys by value.
 * 
 * @param {number} value A number.
 * @param {Object} object An object whose values are numbers.
 * 
 * @returns {Array} An array of words.
 */
function findKeysByValue(value, object) {
    const matchingKeys = [];

    // Loop through the object.
    for (let key in object) {
        let currentValue = object[key];

        if (currentValue === value) {
            matchingKeys.push(key);
        }
    }

    return matchingKeys;
}

/**
 * Gets the highest number from an object whose values are numbers.
 * 
 * @param {Object} object An object.
 * 
 * @returns {number} The highest number.
 */
function getTheHighestValue(object) {
    const arrayOfNumbers = Object.values(object);

    return getBiggestNumber(arrayOfNumbers);
}

/**
 * Finds the biggest number in an array.
 * 
 * @param {Array} array An array of numbers.
 * 
 * @returns {number} The highest number.
 */
function getBiggestNumber(array) {
    function biggest(biggestSoFar, currVal) {
        if (currVal > biggestSoFar) {
            return currVal;
        } else {
            return biggestSoFar;
        }
    }

    return array.reduce(biggest, 0);
}
